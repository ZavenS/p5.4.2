from flask import Flask
from flask_ask import Ask, statement, question

app = Flask(__name__)
ask = Ask(app, '/')


@ask.launch
def start():
    return question('Bonjour, souhaitez-vous consulter votre planning, y ajouter quelque chose ou alors y effacer quelque chose ?')

calendrier = {'lundi': [], 'mardi': [], 'mercredi': [], 'jeudi': [], 'vendredi': [], 'samedi': [], 'dimanche': []}
jours = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

@ask.intent('Ajout')
def choix(jour, acti):
    if jour in jours :
        calendrier[jour].append(acti)
        return statement('Activité ajoutée')
    else:
        return statement('Je ne connais pas ce jour, désolé')


@ask.intent('Effac')
def effTout(jourrr, nombre=None):
    if nombre=='tout':
        if jourrr in jours:
            calendrier[jourrr] = []
            return statement('Toutes les activités de {} ont été effacé.'.format(jourrr))
        else:
            return statement('Je ne connais pas ce jour, désolé')
    else:
        return statement('Il y a eu une erreur, veuillez recommencer')


@ask.intent('EffacerUn')
def eff1(Jour, activite):
    if Jour in jours:
        if activite in calendrier[Jour] :
            calendrier[Jour].remove(activite)
            return statement('Activité effacée')
        else:
            return statement("Cette activité n'était pas prévue, je n'ai donc rien effacé")
    else:
        return statement('Il y a eu une erreur, veuillez recommencer')


@ask.intent('Consult')
def check(jourr):
    if jourr in jours :
        string = ''
        if len(calendrier[jourr]) == 0:
            return statement("Vous n'avez rien de prévu")
        elif len(calendrier[jourr]) == 1:
            return statement('{}, vous allez {}.'.format(jourr, str(calendrier[jourr][0])))
        else:
            for i in range(len(calendrier[jourr])):
                string += calendrier[jourr][i]
                if i == len(calendrier[jourr]) - 1:
                    string += '.'
                else:
                    string += ', '
            return statement('{}, vous allez {}'.format(jourr, string))
    else:
        return statement('Je ne connais pas ce jour, désolé')


if __name__ == '__main__' :
    app.run()
